# 🧪 Elements: Periodic Reference

> Created as part of AppleJam 2020, a 1 week competition to create an iOS app.

This was my first project that I created in SwiftUI and the first app that I shipped on the App Store. 

I really enjoyed working with SwiftUI as it allowed me to turn around this app in a week as a beginner. There was a bit of time left and I used it to create the spinning electron animation in the detail page purely programatically.

<br>

## iOS Frameworks and Technologies Used
- **SwiftUI:** Overall UI for the app
- **[ASCollectionView](https://github.com/apptekstudios/ASCollectionView):** This project was created before LazyHGrid and LazyVGrid were available in SwiftUI; ASCollection View allowed displaying the elements in a grid
- **[SwiftyBeaver](https://github.com/SwiftyBeaver/SwiftyBeaver)**: Logging that is built into iOS currently does not allow for easy log extraction programatically. Using this framework until the functionality is added natively as SwiftyBeaver follows OSLog most closely and allows for an easy transition in the future.
- **[Bowserinator/Periodic-Table-JSON](https://github.com/Bowserinator/Periodic-Table-JSON)**: Data for elements extracted from Wikipedia

## Future Considerations
- Accessibility and Localization
- Transition from using ASCollectionView to SwiftUI LazyHGrid and LazyVGrid instead

<br>

![App Screen Capture](screen-grab.gif)
